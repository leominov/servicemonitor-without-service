package main

import (
	"flag"
	"log"
	"os"
)

var (
	filename = flag.String("f", "", "Path to ServiceMonitor file")
)

func main() {
	flag.Parse()

	monitor, err := ServiceMonitorFromFile(*filename)
	if err != nil {
		log.Fatal(err)
	}

	var (
		validEndpoints   int
		invalidEndpoints int
	)
	for _, endpoint := range monitor.Spec.Endpoints {
		targets, ok := endpoint.Params["target"]
		if !ok || len(targets) == 0 {
			continue
		}
		target := targets[0]
		var foundServiceLabel bool
		for _, relabelConfig := range endpoint.MetricRelabelConfigs {
			if relabelConfig.TargetLabel == "service" && relabelConfig.Replacement != "" {
				validEndpoints++
				foundServiceLabel = true
				break
			}
		}
		if !foundServiceLabel {
			invalidEndpoints++
			log.Println(target)
		}
	}

	log.Printf("Endpoints stats (valid/invalid): %d/%d", validEndpoints, invalidEndpoints)

	if invalidEndpoints > 0 {
		os.Exit(1)
	}
}
