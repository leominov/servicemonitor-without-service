# servicemonitor-without-service

Поиск Endpoint'ов в ServiceMonitor, в которых не указан реврайт `service` в списке `MetricRelabelings`.

## Пример отчета

```
2021/12/14 11:31:54 https://qlean.ru
2021/12/14 11:31:54 https://api.qlean.ru
2021/12/14 11:31:54 https://crm.qlean.ru/users
2021/12/14 11:31:54 https://hrm.qlean.ru/queue/1
2021/12/14 11:31:54 https://cdn.cloud.qlean.ru/master/header.json
2021/12/14 11:31:54 https://getwola.ru
2021/12/14 11:31:54 https://blog.qlean.ru/wp-login.php
2021/12/14 11:31:54 https://remont-app.qlean.ru
2021/12/14 11:31:54 https://stirka.qlean.ru
2021/12/14 11:31:54 https://stirka.qlean.ru/admin/authorization/new
2021/12/14 11:31:54 Endpoints stats (valid/invalid): 43/10
```
