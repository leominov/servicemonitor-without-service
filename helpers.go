package main

import (
	"os"

	v1 "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	"k8s.io/apimachinery/pkg/util/yaml"
)

func ServiceMonitorFromFile(filename string) (*v1.ServiceMonitor, error) {
	b, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	sm := &v1.ServiceMonitor{}
	return sm, yaml.Unmarshal(b, sm)
}
